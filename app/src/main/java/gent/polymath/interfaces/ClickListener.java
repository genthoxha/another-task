package gent.polymath.interfaces;

import android.view.View;

/**
 * Created by genthoxha on 1/1/2018.
 */

public interface ClickListener {
    public void onClick(View view, int position);
    public void onLongClick(View view, int position);
}
