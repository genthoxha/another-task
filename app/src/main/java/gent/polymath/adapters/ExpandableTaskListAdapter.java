package gent.polymath.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import gent.polymath.R;
import gent.polymath.models.TaskerListModel;
import gent.polymath.models.TaskerModel;

/**
 * Created by genthoxha on 1/1/2018.
 */

public class ExpandableTaskListAdapter extends BaseExpandableListAdapter {


    private Context context;
    private List<TaskerListModel> listDataHeaders;
    private HashMap<TaskerListModel, List<TaskerModel>> listHashMap;
    private ImageView arrow;


    public ExpandableTaskListAdapter(Context context, List<TaskerListModel> listDataHeaders, HashMap<TaskerListModel, List<TaskerModel>> listHashMap) {
        this.context = context;
        this.listDataHeaders = listDataHeaders;
        this.listHashMap = listHashMap;
    }


    @Override
    public int getGroupCount() {
        if (listDataHeaders == null)
            return 0;
        else
            return listDataHeaders.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (!(getGroupTypeCount() == 0)) {
            return listHashMap.get(listDataHeaders.get(groupPosition)).size();
        } else {
            return 0;
        }
    }


    @Override
    public Object getGroup(int groupPosition) {
        return listDataHeaders.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return listHashMap.get(listDataHeaders.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        TaskerListModel taskList = (TaskerListModel) getGroup(groupPosition);
        if (listDataHeaders.get(groupPosition).isValid()) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.list_header, null);
            }

            TextView listName = (TextView) convertView.findViewById(R.id.listTitle);
            TextView listDescription = (TextView) convertView.findViewById(R.id.listDescription);
            TextView listId = (TextView) convertView.findViewById(R.id.listId);


            arrow = (ImageView) convertView.findViewById(R.id.arrow);
            if (getGroupTypeCount() != 0) {
                listName.setText(taskList.getListName());
                listDescription.setText(taskList.getListDescription());
                listId.setText(String.format("%s.", String.valueOf(taskList.getId())));
            }
        }


        return convertView;
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);

    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);

    }

    @Override
    public int getGroupTypeCount() {
        return super.getGroupTypeCount();
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        TaskerModel tasker = (TaskerModel) getChild(groupPosition, childPosition);


        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.task_list, null);
        }


        TextView taskTitle = convertView.findViewById(R.id.taskTitle);
        TextView taskDueDate = convertView.findViewById(R.id.taskDueDate);
        TextView taskId = convertView.findViewById(R.id.taskNumber);
        TextView taskDescription = convertView.findViewById(R.id.taskDescription);


        taskTitle.setText((tasker.getTaskTitle()));
        taskDueDate.setText((tasker.getTaskDueDate()));
        taskId.setText(String.format("%d.", tasker.getId()));
        taskDescription.setText(tasker.getTaskDescription());

        return convertView;
    }

    @Override
    public int getChildType(int groupPosition, int childPosition) {
        return super.getChildType(groupPosition, childPosition);
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
