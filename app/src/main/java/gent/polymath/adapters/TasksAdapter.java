package gent.polymath.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import gent.polymath.R;
import gent.polymath.models.TaskerModel;

/**
 * Created by genthoxha on 12/30/2017.
 */

public class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.TasksAdapterViewHolder> {

    private List<TaskerModel> tasks;
    private Activity activity;

    public TasksAdapter(List<TaskerModel> tasks, Activity activity  ) {

        this.tasks = tasks;
        this.activity = activity;
    }

    public TaskerModel getItem(int position) {
        return tasks.get(position);
    }

    @Override
    public TasksAdapter.TasksAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_list_row, parent, false);
        return new TasksAdapterViewHolder(view);
    }


    @Override
    public void onBindViewHolder(TasksAdapterViewHolder holder, int position) {


        TaskerModel task = tasks.get(position);

        holder.taskTitle.setText(task.getTaskTitle());
        holder.taskDueDate.setText(task.getTaskDueDate());
        holder.taskNumber.setText((task.getId()+"."));
        holder.taskDescription.setText(task.getTaskDescription());

    }

    @Override
    public int getItemCount() {
        if (tasks != null) {
            return tasks.size();
        } else {
            return 0;
        }
    }

    class TasksAdapterViewHolder extends RecyclerView.ViewHolder {
        TextView taskTitle;
        TextView taskDueDate;
        TextView taskNumber;
        TextView taskDescription;
        TasksAdapterViewHolder(View view) {
            super(view);
            setIsRecyclable(false);
            taskDueDate = (TextView) view.findViewById(R.id.taskDueDate);
            taskTitle = (TextView) view.findViewById(R.id.taskTitle);
            taskNumber = (TextView) view.findViewById(R.id.taskNumber);
            taskDescription = (TextView) view.findViewById(R.id.taskDescription);
        }
    }


}


