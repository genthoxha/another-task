package gent.polymath.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import gent.polymath.fragments.ListFragment;
import gent.polymath.fragments.TaskFragment;
import gent.polymath.fragments.CompletedStatement;

/**
 * Created by genthoxha on 12/28/2017.
 */

public class PagerAdapter extends FragmentPagerAdapter {

    int numberOfTabs;

    public PagerAdapter(FragmentManager fm, int numberOfTabs) {
        super(fm);
        this.numberOfTabs = numberOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {

            case 0:
                TaskFragment f1 = new TaskFragment();
                return f1;
            case 1:
                ListFragment f2 = new ListFragment();
                return f2;
            case 2:
                CompletedStatement f3 = new CompletedStatement();
                return f3;
            default:
                return null;

        }
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
