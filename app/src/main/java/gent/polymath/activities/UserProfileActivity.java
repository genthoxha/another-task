package gent.polymath.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import gent.polymath.R;

/**
 * Created by genthoxha on 1/26/2018.
 */

public class UserProfileActivity extends AppCompatActivity {

    TextView userEmail;
    TextView userName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.userprofile_activity);


        userEmail = (TextView) findViewById(R.id.emailTxtView);
        userName = (TextView) findViewById(R.id.nameTxtView);

        if (getIntent().getExtras() != null) {

            userEmail.setText(getIntent().getExtras().getString("userEmail"));
            userName.setText(getIntent().getExtras().getString("name"));
            if (getIntent().getExtras().getString("userEmail") == null) {
                userEmail.setVisibility(View.GONE);
            }
            if (getIntent().getExtras().getString("name") == null) {
                userName.setVisibility(View.GONE);
            }


        }


    }
}
