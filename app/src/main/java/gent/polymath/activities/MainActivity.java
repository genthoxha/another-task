package gent.polymath.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.widget.Button;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;

import gent.polymath.R;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MainActivity extends AppCompatActivity {

    private Button fbLoginBtn;
    private CallbackManager callbackManager;
    private FirebaseAuth firebaseAuth;
    private SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        Realm.init(this);
        Realm realm;
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().
                name("anotherTaskDb").
                schemaVersion(1).
                build();
        Realm.setDefaultConfiguration(realmConfiguration);
        realm = Realm.getDefaultInstance();

        fbLoginBtn = (LoginButton) findViewById(R.id.facebookLoginButton);
        callbackManager = CallbackManager.Factory.create();
        firebaseAuth = FirebaseAuth.getInstance();

        checkForLogin();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        Intent i = new Intent(MainActivity.this, ProfileActivity.class);
                        i.putExtra("User", loginResult.getAccessToken().getUserId() + "\n" + loginResult.getAccessToken().getToken());


                        startActivity(i);
                        finish();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });

    }

    private void checkForLogin() {
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void btnLoginClick(View v) {
        Intent i = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(i);
    }
    public void btnRegistrationClick(View v) {
        Intent i = new Intent(MainActivity.this, RegistrationActivity.class);
        startActivity(i);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode,
                resultCode, data);
    }

    @Override
    protected void onStart() {
        super.onStart();


        sharedPreferences = getSharedPreferences("loginstatus", MODE_PRIVATE);
        Boolean b = sharedPreferences.getBoolean("login", false);
        if (b) {
            Intent i = new Intent(this, ProfileActivity.class);
            startActivity(i);
        }





    }
}
