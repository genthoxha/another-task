package gent.polymath.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import gent.polymath.R;
import gent.polymath.fragments.EditTaskFragment;
import gent.polymath.fragments.SingleTaskFragment;
import io.realm.Realm;

/**
 * Created by genthoxha on 1/12/2018.
 */

public class SingleActivityContent extends AppCompatActivity {

    Realm realm;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_activity_layout);

        SingleTaskFragment singleTaskFragment = new SingleTaskFragment();

        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.replace(R.id.content_frame, singleTaskFragment);
        fragmentTransaction.commit();



        realm = Realm.getDefaultInstance();


    }


}





