package gent.polymath.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import gent.polymath.R;
import gent.polymath.adapters.PagerAdapter;
import gent.polymath.fragments.CompletedStatement;
import gent.polymath.fragments.ListFragment;
import gent.polymath.fragments.TaskFragment;
import gent.polymath.models.User;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by genthoxha on 12/26/2017.
 */

public class ProfileActivity extends AppCompatActivity implements
        TaskFragment.OnFragmentInteractionListener,
        ListFragment.OnFragmentInteractionListener,
        CompletedStatement.OnFragmentInteractionListener {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_content);

        sharedPreferences = this.getSharedPreferences("loginstatus", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putBoolean("login", true);
        editor.apply();

        realm = Realm.getDefaultInstance();

     /*   String userid = null;
        if (getIntent().getExtras() != null) {
             userid = getIntent().getExtras().getString("User", "");
        }


        realm = Realm.getDefaultInstance();
        realm.where(User.class).equalTo("userId", "");

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name(userid + ".realm")
                .build();*/

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setText("Task"));
        tabLayout.addTab(tabLayout.newTab().setText("List"));
        tabLayout.addTab(tabLayout.newTab().setText("Marked"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        return;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.logoutItem) {
            dologout();
        } else if (id == R.id.profileItem) {


            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
            FirebaseUser user = firebaseAuth.getCurrentUser();
            if (user != null) {
                Intent i = new Intent(this, UserProfileActivity.class);
                i.putExtra("userEmail", user.getEmail());
                i.putExtra("name", user.getDisplayName());
                startActivity(i);
            }

        }

        return true;


    }

    private void dologout() {
        LoginManager.getInstance().logOut();
        Intent i = new Intent(ProfileActivity.this, MainActivity.class);
        editor = sharedPreferences.edit();
        editor.putBoolean("login", false);
        editor.apply();
        startActivity(i);
        finish();

    }


}
