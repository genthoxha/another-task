package gent.polymath.models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by genthoxha on 12/28/2017.
 */

public class SubTask extends RealmObject{

    @PrimaryKey
    private int id;
    private String subTaskTitle;
    private String subTaskDescription;
    private String subTaskDueDate;
    private Boolean subTaskCompleted;
    private RealmList<String> lists;

    public int getSubTaskId() {
        return id;
    }

    public void setSubTaskId(int subTaskId) {
        this.id = subTaskId;
    }

    public String getSubTaskTitle() {
        return subTaskTitle;
    }

    public void setSubTaskTitle(String subTaskTitle) {
        this.subTaskTitle = subTaskTitle;
    }

    public String getSubTaskDescription() {
        return subTaskDescription;
    }

    public void setSubTaskDescription(String subTaskDescription) {
        this.subTaskDescription = subTaskDescription;
    }

    public String getSubTaskDueDate() {
        return subTaskDueDate;
    }

    public void setSubTaskDueDate(String subTaskDueDate) {
        this.subTaskDueDate = subTaskDueDate;
    }

    public Boolean getSubTaskCompleted() {
        return subTaskCompleted;
    }

    public void setSubTaskCompleted(Boolean subTaskCompleted) {
        this.subTaskCompleted = subTaskCompleted;
    }

    public RealmList<String> getLists() {
        return lists;
    }

    public void setLists(RealmList<String> lists) {
        this.lists = lists;
    }
}
