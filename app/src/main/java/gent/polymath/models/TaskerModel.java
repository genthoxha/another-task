package gent.polymath.models;


import java.io.Serializable;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by genthoxha on 12/28/2017.
 */

public class TaskerModel extends RealmObject  {


    @PrimaryKey
    private int id;
    private String taskTitle;
    private String taskDescription;
    private String taskDueDate;
    private Boolean completed;
    private Boolean hasSubTask;
    private SubTask subTask;
    private RealmList<String> lists;


    public TaskerModel(int id, String taskTitle, String taskDescription, String taskDueDate, Boolean completed, Boolean hasSubTask, SubTask subTask, RealmList<String> lists) {
        this.id = id;
        this.taskTitle = taskTitle;
        this.taskDescription = taskDescription;
        this.taskDueDate = taskDueDate;
        this.completed = completed;
        this.hasSubTask = hasSubTask;
        this.subTask = subTask;
        this.lists = lists;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TaskerModel() {

    }

    public SubTask getSubTask() {
        return subTask;
    }

    public void setSubTask(SubTask subTask) {
        this.subTask = subTask;
    }

    public Boolean getHasSubTask() {
        return hasSubTask;
    }

    public void setHasSubTask(Boolean hasSubTask) {
        this.hasSubTask = hasSubTask;
    }

    public String getTaskTitle() {
        return taskTitle;
    }

    public void setTaskTitle(String taskTitle) {
        this.taskTitle = taskTitle;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public String getTaskDueDate() {
        return taskDueDate;
    }

    public void setTaskDueDate(String taskDueDate) {
        this.taskDueDate = taskDueDate;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }


    public RealmList<String> getLists() {
        return lists;
    }

    public void setLists(RealmList<String> lists) {
        this.lists = lists;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TaskerModel) {
            TaskerModel t = (TaskerModel) obj;
            return id == ((TaskerModel) obj).getId() && taskTitle.equals(((TaskerModel) obj).getTaskTitle());
        }
        return false;
    }
}
