package gent.polymath.models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by genthoxha on 12/28/2017.
 */

public class TaskerListModel extends RealmObject {
    @PrimaryKey
    private int id;
    private String listName;
    private String listDescription;
    private RealmList<TaskerModel> tasks = new RealmList<>();


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getListName() {
        if (listName.isEmpty() || listName.trim().isEmpty() || listName == null ) {
            return null;

        } else {
            return listName;
        }
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public String getListDescription() {
        return listDescription;
    }

    public void setListDescription(String listDescription) {
        this.listDescription = listDescription;
    }

    public RealmList<TaskerModel> getTasks() {
        return tasks;
    }

    public void setTasks(RealmList<TaskerModel> tasks) {
        this.tasks.addAll(tasks);
    }
}
