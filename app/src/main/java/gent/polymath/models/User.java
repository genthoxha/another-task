package gent.polymath.models;

import io.realm.RealmObject;

/**
 * Created by genthoxha on 1/26/2018.
 */

public class User extends RealmObject{
    
    private String name;
    private String email;
    private String userId;

    public String getUserId() {
        return userId;
    }

    public User() {

    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
