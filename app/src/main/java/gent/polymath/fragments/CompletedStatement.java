package gent.polymath.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import gent.polymath.R;
import gent.polymath.activities.SingleActivityContent;
import gent.polymath.adapters.CompletedStatmentAdapter;
import gent.polymath.interfaces.ClickListener;
import gent.polymath.interfaces.RecyclerTouchListener;
import gent.polymath.models.TaskerModel;
import io.realm.Realm;
import io.realm.RealmResults;


public class CompletedStatement extends Fragment {

    CompletedStatmentAdapter completedAdapter;
    CompletedStatmentAdapter unCompletedAdpater;
    RecyclerView cRecyclerView;
    RecyclerView uRecyclerView;
    Realm realm;

    RealmResults<TaskerModel> cTasks;
    RealmResults<TaskerModel> uTasks;

    private OnFragmentInteractionListener mListener;

    public CompletedStatement() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_tab3, container, false);


        cRecyclerView = (RecyclerView) v.findViewById(R.id.markedAsCompleted);
        uRecyclerView = (RecyclerView) v.findViewById(R.id.markedAsUncompleted);
        realm = Realm.getDefaultInstance();

        cTasks = realm.where(TaskerModel.class).equalTo("completed", true).findAll();

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        completedAdapter = new CompletedStatmentAdapter(cTasks, getActivity());
        cRecyclerView.setLayoutManager(llm);
        cRecyclerView.setAdapter(completedAdapter);

        uTasks = realm.where(TaskerModel.class).equalTo("completed", false).findAll();
        LinearLayoutManager ulm = new LinearLayoutManager(getActivity());
        unCompletedAdpater = new CompletedStatmentAdapter(uTasks, getActivity());
        uRecyclerView.setLayoutManager(ulm);
        uRecyclerView.setAdapter(unCompletedAdpater);


        cRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), cRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                TaskerModel item = completedAdapter.getItem(position);
                Intent i = new Intent(getActivity(), SingleActivityContent.class);
                String primarykey = null;
                primarykey = String.valueOf(item.getId());
                i.putExtra("id", primarykey);
                startActivity(i);

            }

            @Override
            public void onLongClick(View view, int position) {
                TaskerModel item = completedAdapter.getItem(position);
                setUnCompleted(item, v);
            }

        }));

        uRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), uRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                TaskerModel item = unCompletedAdpater.getItem(position);
                Intent i = new Intent(getActivity(), SingleActivityContent.class);
                String primarykey = null;
                primarykey = String.valueOf(item.getId());
                i.putExtra("id", primarykey);
                startActivity(i);

            }

            @Override
            public void onLongClick(View view, int position) {

                TaskerModel item = unCompletedAdpater.getItem(position);
                setCompleted(item,v);


            }

        }));


        return v;
    }


    private void setUnCompleted(final TaskerModel taskerModel, final View v) {

        final AlertDialog.Builder myDialog = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        myDialog.setMessage("Do you want to set as uncompleted task ?");
        myDialog.create();
        myDialog.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                realm.beginTransaction();
                TaskerModel model = realm.where(TaskerModel.class).equalTo("id",taskerModel.getId()).findFirst();
                if (model != null) {
                    model.setCompleted(false);
                    realm.copyToRealmOrUpdate(model);
                }
                realm.commitTransaction();
                completedAdapter = new CompletedStatmentAdapter(cTasks, getActivity());
                cRecyclerView.setAdapter(completedAdapter);
                cRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                cRecyclerView.setItemAnimator(new DefaultItemAnimator());

                unCompletedAdpater = new CompletedStatmentAdapter(uTasks, getActivity());
                uRecyclerView.setAdapter(unCompletedAdpater);
                uRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                uRecyclerView.setItemAnimator(new DefaultItemAnimator());
            }
        });
        myDialog.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        myDialog.show();
    }
    private void setCompleted(final TaskerModel taskerModel, final View v) {

        final AlertDialog.Builder myDialog = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        myDialog.setMessage("Do you want to set as completed selected task ?");
        myDialog.create();
        myDialog.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                realm.beginTransaction();
                TaskerModel model = realm.where(TaskerModel.class).equalTo("id",taskerModel.getId()).findFirst();
                if (model != null) {
                    model.setCompleted(true);
                    realm.copyToRealmOrUpdate(model);
                }
                realm.commitTransaction();
                unCompletedAdpater = new CompletedStatmentAdapter(uTasks, getActivity());
                uRecyclerView.setAdapter(unCompletedAdpater);
                uRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                uRecyclerView.setItemAnimator(new DefaultItemAnimator());

                completedAdapter = new CompletedStatmentAdapter(cTasks, getActivity());
                cRecyclerView.setAdapter(completedAdapter);
                cRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                cRecyclerView.setItemAnimator(new DefaultItemAnimator());
            }
        });
        myDialog.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        myDialog.show();
    }









    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
