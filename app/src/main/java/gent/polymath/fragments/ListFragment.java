package gent.polymath.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import gent.polymath.R;
import gent.polymath.activities.SingleActivityContent;
import gent.polymath.adapters.ExpandableTaskListAdapter;
import gent.polymath.adapters.TasksAdapter;
import gent.polymath.interfaces.ClickListener;
import gent.polymath.interfaces.RecyclerTouchListener;
import gent.polymath.models.TaskerListModel;
import gent.polymath.models.TaskerModel;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;


public class ListFragment extends Fragment {


    RecyclerView allTasksRecyclerView;
    RecyclerView addedTasksRecyclerView;
    private OnFragmentInteractionListener mListener;
    private ExpandableListView expandableListView;
    private FloatingActionButton floatingActionButton;
    private Realm realm;
    private Button saveListBtn;
    private LinearLayout linearLayout;
    private List<TaskerListModel> listDataHeaders;
    private HashMap<TaskerListModel, List<TaskerModel>> listHashMap;
    private TasksAdapter tasksAdapter;
    private EditText listTitletxt;
    private EditText listDescriptiontxt;
    private ArrayList<TaskerModel> selectedItems;
    private ExpandableTaskListAdapter expandableTaskListAdapter;


    public ListFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();


    }


    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);
        initData(view);
        showAllTasksRecyclerView(getActivity());
        selectedItems = new ArrayList<>();

        saveListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveListOnDb(getContext());
                showExpandableListView();
            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (linearLayout.getVisibility() == View.VISIBLE) {
                    linearLayout.setVisibility(View.INVISIBLE);
                    expandableListView.setVisibility(View.VISIBLE);
                    floatingActionButton.setImageResource(R.drawable.ic_add_circle_outline_black_24dp);
                } else {
                    floatingActionButton.setImageResource(R.drawable.ic_arrow_back_black_24dp);
                    expandableListView.setVisibility(View.INVISIBLE);
                    linearLayout.setVisibility(View.VISIBLE);
                }


            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view,
                                        int groupPosition, int childPosition, long l) {
                TaskerModel item = (TaskerModel) expandableTaskListAdapter.getChild(groupPosition,childPosition);
                Intent i = new Intent(getActivity(), SingleActivityContent.class);
                String primarykey = null;
                primarykey = String.valueOf(item.getId());
                i.putExtra("id", primarykey);
                startActivity(i);
                return false;
            }
        });


        expandableListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                showDialog(position);
                showExpandableListView();
                return false;
            }
        });
        allTasksRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), allTasksRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                addTaskToItems(position);
            }

            @Override
            public void onLongClick(View view, int position) {


            }
        }));
        addedTasksRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), allTasksRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                selectedItems.remove(position);
                addedTasksRecyclerView.invalidate();
                tasksAdapter = new TasksAdapter(selectedItems, getActivity());
                addedTasksRecyclerView.setAdapter(tasksAdapter);
                addedTasksRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                addedTasksRecyclerView.setItemAnimator(new DefaultItemAnimator());
            }

            @Override
            public void onLongClick(View view, int position) {


            }
        }));
        showExpandableListView();
        return view;
    }

    private void addTaskToItems(int position) {
        RealmResults<TaskerModel> allTasks = realm.where(TaskerModel.class).findAll();
        for (TaskerModel taskerModel : allTasks) {
            if (taskerModel.getId() == position + 1) {
                selectedItems.add(taskerModel);
            }
        }
        tasksAdapter = new TasksAdapter(selectedItems, getActivity());
        addedTasksRecyclerView.setAdapter(tasksAdapter);
        addedTasksRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        addedTasksRecyclerView.setItemAnimator(new DefaultItemAnimator());

    }

    public List<TaskerModel> updateRows(List<TaskerModel> taskerModelList) {
        realm.beginTransaction();
        for (int i = 0; i < taskerModelList.size(); i++) {
            taskerModelList.get(i).setId(i + 1);
        }
        realm.commitTransaction();
        return taskerModelList;

    }

    public void initData(View view) {
        addedTasksRecyclerView = (RecyclerView) view.findViewById(R.id.addedTasksRecyclerView);
        allTasksRecyclerView = (RecyclerView) view.findViewById(R.id.allTasks);
        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.floatActionBtn);
        linearLayout = (LinearLayout) view.findViewById(R.id.newListForm);
        expandableListView = (ExpandableListView) view.findViewById(R.id.allList);
        listTitletxt = (EditText) view.findViewById(R.id.listTitle);
        listDescriptiontxt = (EditText) view.findViewById(R.id.listDescription);
        saveListBtn = (Button) view.findViewById(R.id.saveListBtn);

    }


    public void showAllTasksRecyclerView(Context context) {
        realm.beginTransaction();
        RealmResults<TaskerModel> allTasks = realm.where(TaskerModel.class).findAll();
        tasksAdapter = new TasksAdapter(allTasks, getActivity());
        allTasksRecyclerView.setAdapter(tasksAdapter);
        allTasksRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        allTasksRecyclerView.setItemAnimator(new DefaultItemAnimator());
        realm.commitTransaction();

    }

    public void saveListOnDb(final Context context) {
        realm.beginTransaction();
        TaskerListModel realmObject = realm.createObject(TaskerListModel.class, setPrimaryKey());
        RealmList<TaskerListModel> taskerListModels = new RealmList<>();
        RealmList<TaskerModel> taskers = new RealmList<>();
        realm.commitTransaction();
        realm.beginTransaction();
        realmObject.setListName(listTitletxt.getText().toString());
        realmObject.setListDescription(listDescriptiontxt.getText().toString());
        taskers.addAll(selectedItems);
        realmObject.setTasks(taskers);
        realm.commitTransaction();
        Toast.makeText(context, "List saved successfully", Toast.LENGTH_SHORT).show();
    }



    public List<TaskerListModel> deleteListFromDb(final int position) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<TaskerListModel> result = realm.where(TaskerListModel.class).equalTo("id", position + 1).findAll();
                result.deleteAllFromRealm();

            }
        });
        realm.beginTransaction();
        RealmResults<TaskerListModel> alldata = realm.where(TaskerListModel.class).findAll();
        List<TaskerListModel> t = realm.copyFromRealm(alldata);
        for (int i = 0; i < t.size(); i++) {
            t.get(i).setId(i + 1);
        }
        alldata.deleteAllFromRealm();
        RealmList<TaskerListModel> newData = new RealmList<>();
        newData.addAll(t);
        realm.copyToRealmOrUpdate(newData);
        realm.commitTransaction();
        expandableListView.invalidate();
        showExpandableListView();
        return newData;
    }


    public void showExpandableListView() {
        RealmResults<TaskerListModel> allData = realm.where(TaskerListModel.class).findAll();
        listDataHeaders = new ArrayList<>();
        listDataHeaders.addAll(allData);
        listHashMap = new HashMap<>();
        for (TaskerListModel taskerListModel : allData) {
            listHashMap.put(taskerListModel, taskerListModel.getTasks());
        }
        expandableTaskListAdapter = new ExpandableTaskListAdapter(getActivity(), listDataHeaders, listHashMap);
        expandableListView.setGroupIndicator(null);
        expandableListView.setAdapter(expandableTaskListAdapter);
    }


    public int setPrimaryKey() {
        Number id = realm.where(TaskerListModel.class).max("id");
        return (id == null) ? 1 : id.intValue() + 1;
    }

    private void showDialog(final int position) {
        final AlertDialog.Builder myDialog = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        myDialog.setMessage("Do you want to delete selected list ?");
        myDialog.create();
        myDialog.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteListFromDb(position);
            }
        });
        myDialog.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });


        myDialog.show();

    }

    // Nuk lejon double data
    public Boolean titleExist(String tasker, List<TaskerModel> taskerModelList) {
        for (int i = 0; i < taskerModelList.size(); i++) {
            if (taskerModelList.get(i).getTaskTitle().equals(tasker)) {
                return true;
            }
        }
        return false;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
