package gent.polymath.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import gent.polymath.R;
import gent.polymath.models.SubTask;
import gent.polymath.models.TaskerModel;
import io.realm.Realm;

/**
 * Created by genthoxha on 1/16/2018.
 */


public class SingleTaskFragment extends Fragment {


    TaskerModel taskerModel;
    SubTask subTaskModel;
    Button editBtn;
    Realm realm;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.singletask_fragment, null);
        editBtn = (Button) view.findViewById(R.id.editTask);

        realm = Realm.getDefaultInstance();
        String key = null;
        if (getActivity().getIntent().getExtras() != null) {
            key = getActivity().getIntent().getExtras().getString("id");
        }
        if (key != null) {
            taskerModel = realm.where(TaskerModel.class).equalTo("id", Integer.parseInt(key)).findFirst();
            if (taskerModel != null) {
                if (taskerModel.getHasSubTask()) {
                    subTaskModel = taskerModel.getSubTask();
                    setDataIntoView(taskerModel, subTaskModel, view);
                } else {
                    setDataIntoView(taskerModel, subTaskModel, view);
                }
            }
        }
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.content_frame, new EditTaskFragment());
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    private void setDataIntoView(TaskerModel taskerModel, SubTask subTaskModel, View v) {

        TextView tTitle = v.findViewById(R.id.txtViewTaskTitle);
        TextView tDescription = v.findViewById(R.id.txtViewTaskDescription);
        TextView tCheck = v.findViewById(R.id.txtViewTaskChecker);
        TextView tDueDate = v.findViewById(R.id.txtViewTaskDueDate);

        TextView sTitle = v.findViewById(R.id.txtViewsubTaskTitle);
        TextView sDescription = v.findViewById(R.id.txtViewSubtaskDescription);
        TextView sCheck = v.findViewById(R.id.txtViewsubTaskChecker);
        TextView sDueDate = v.findViewById(R.id.txtViewSubTaskDueDate);

        LinearLayout linearLayout = v.findViewById(R.id.subtaskLinearLayout);

        tTitle.setText(taskerModel.getTaskTitle());
        tDescription.setText(taskerModel.getTaskDescription());
        tCheck.setText(String.format("Task completed: %s", taskerModel.getCompleted().toString()));
        tDueDate.setText(String.format("Task due date: %s", taskerModel.getTaskDueDate()));

        if (taskerModel.getHasSubTask()) {
            linearLayout.setVisibility(View.VISIBLE);
            if (subTaskModel.getSubTaskTitle() != null) {
                sTitle.setText(subTaskModel.getSubTaskTitle());
            }
            if (subTaskModel.getSubTaskDescription() != null) {
                sDescription.setText(subTaskModel.getSubTaskDescription());
            }

            if (subTaskModel.getSubTaskCompleted() != null) {
                sCheck.setText(String.format("Completed: %s", subTaskModel.getSubTaskCompleted().toString()));
            } else {
                sCheck.setVisibility(View.GONE);
            }
            if (subTaskModel.getSubTaskDueDate() != null) {
                sDueDate.setText(String.format("Subtask due date: %s", subTaskModel.getSubTaskDueDate()));
            } else {
                sDueDate.setVisibility(View.GONE);
            }
        } else {
            linearLayout.setVisibility(View.GONE);
        }


    }


}
