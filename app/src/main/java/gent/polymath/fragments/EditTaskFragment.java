package gent.polymath.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;

import gent.polymath.R;
import gent.polymath.activities.MainActivity;
import gent.polymath.activities.ProfileActivity;
import gent.polymath.activities.SingleActivityContent;
import gent.polymath.adapters.TasksAdapter;
import gent.polymath.models.SubTask;
import gent.polymath.models.TaskerModel;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by genthoxha on 1/16/2018.
 */

public class EditTaskFragment extends Fragment {


    EditText tTitle;
    EditText tDescription;
    CheckBox tCheck;
    EditText tDueDate;

    Button deleteBtn;
    Button deleteSubTaskBtn;
    Button saveBtn;


    EditText sTitle;
    EditText sDescription;
    CheckBox sCheck;
    EditText sDueDate;
    Realm realm;
    TaskerModel taskerModel;
    SubTask subTaskModel;
    LinearLayout linearLayout;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.edittask_fragment, null);
        realm = Realm.getDefaultInstance();


        deleteBtn = (Button) v.findViewById(R.id.deleteTask);
        deleteSubTaskBtn = (Button) v.findViewById(R.id.deleteSubTask);
        saveBtn = (Button) v.findViewById(R.id.saveTask);


        String key = null;
        if (getActivity().getIntent().getExtras() != null) {
            key = getActivity().getIntent().getExtras().getString("id");
        }
        if (key != null) {
            taskerModel = realm.where(TaskerModel.class).equalTo("id", Integer.parseInt(key)).findFirst();
            if (taskerModel != null) {
                if (taskerModel.getHasSubTask() != null) {

                    subTaskModel = taskerModel.getSubTask();
                }
                setDataIntoView(taskerModel, subTaskModel, v);




                saveBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        realm.beginTransaction();
                        taskerModel.setTaskTitle(tTitle.getText().toString());
                        taskerModel.setTaskDescription(tDescription.getText().toString());
                        taskerModel.setCompleted(tCheck.isChecked());
                        SubTask subTask = realm.createObject(SubTask.class, setSubTaskPrimaryKey());

                        if (linearLayout.isShown()) {
                            if (subTaskModel == null) {
                                if (!sTitle.getText().toString().isEmpty() && !sTitle.getText().toString().trim().equals(null)) {
                                    subTask.setSubTaskTitle(sTitle.getText().toString());
                                }
                                if (!sDescription.getText().toString().isEmpty() && !sDescription.getText().toString().trim().equals(null)) {
                                    subTask.setSubTaskDescription(sDescription.getText().toString());
                                }
                                if (!sDueDate.getText().toString().isEmpty() && !sDueDate.getText().toString().trim().equals(null)) {
                                    subTask.setSubTaskDueDate(sDueDate.getText().toString());
                                }
                                if (sCheck.isChecked()) {
                                    subTask.setSubTaskCompleted(true);
                                } else {
                                    subTask.setSubTaskCompleted(false);
                                }
                            } else {
                                if (!sTitle.getText().toString().isEmpty() && !sTitle.getText().toString().trim().equals(null)) {
                                    subTaskModel.setSubTaskTitle(sTitle.getText().toString());
                                }
                                if (!sDescription.getText().toString().isEmpty() && !sDescription.getText().toString().trim().equals(null)) {
                                    subTaskModel.setSubTaskDescription(sDescription.getText().toString());
                                }
                                if (!sDueDate.getText().toString().isEmpty() && !sDueDate.getText().toString().trim().equals(null)) {
                                    subTaskModel.setSubTaskDueDate(sDueDate.getText().toString());
                                }
                                if (sCheck.isChecked()) {
                                    subTask.setSubTaskCompleted(true);
                                } else {
                                    subTask.setSubTaskCompleted(false);
                                }
                            }

                            if (subTaskModel != null) {

                                realm.copyToRealmOrUpdate(subTaskModel);
                                taskerModel.setSubTask(subTaskModel);
                                taskerModel.setHasSubTask(true);

                            } else {
                                subTaskModel = subTask;
                                taskerModel.setSubTask(subTaskModel);
                                taskerModel.setHasSubTask(false);

                            }

                        }
                        realm.copyToRealmOrUpdate(taskerModel);

                        realm.commitTransaction();
                        Intent i = new Intent(getActivity(), ProfileActivity.class);
                        startActivity(i);

                    }
                });


                deleteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


//                        deleteItemFromDb(taskerModel);
                        showFailPopup(v, taskerModel);
                        Toast.makeText(getActivity(), "Task deleted successfully", Toast.LENGTH_SHORT).show();


                    }
                });

                deleteSubTaskBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        realm.beginTransaction();
                        if (subTaskModel != null) {
                            subTaskModel.deleteFromRealm();
                            taskerModel.setHasSubTask(false);
                            realm.commitTransaction();

                        }

                        linearLayout.setVisibility(View.GONE);

                    }
                });

            }


        }
        return v;
    }

    public void deleteItemFromDb(final TaskerModel taskerModel) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<TaskerModel> result = realm.where(TaskerModel.class).equalTo("id", taskerModel.getId()).findAll();
                result.deleteAllFromRealm();
            }
        });
        realm.beginTransaction();
        RealmResults<TaskerModel> alldata = realm.where(TaskerModel.class).findAll();
        List<TaskerModel> t = realm.copyFromRealm(alldata);
        for (int i = 0; i < t.size(); i++) {
            t.get(i).setId(i + 1);
        }
        alldata.deleteAllFromRealm();
        RealmList<TaskerModel> newData = new RealmList<>();
        newData.addAll(t);
        realm.copyToRealmOrUpdate(newData);
        realm.commitTransaction();

    }
    private void showFailPopup(final View v, final TaskerModel taskerModel) {

        final AlertDialog.Builder myDialog = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        myDialog.setMessage("Do you want to delete selected task ?");
        myDialog.create();
        myDialog.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteItemFromDb(taskerModel);
                Intent i = new Intent(getActivity(), ProfileActivity.class);
                startActivity(i);


            }
        });
        myDialog.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        myDialog.show();
    }


    public int setSubTaskPrimaryKey() {
        Number id = realm.where(SubTask.class).max("id");
        return (id == null) ? 1 : id.intValue() + 1;
    }


    private void setDataIntoView(TaskerModel taskerModel, SubTask subTaskModel, View v) {

        tTitle = (EditText) v.findViewById(R.id.txtViewTaskTitle);
        tDescription = (EditText) v.findViewById(R.id.txtViewTaskDescription);
        tCheck = (CheckBox) v.findViewById(R.id.editCheck);
        tDueDate = (EditText) v.findViewById(R.id.txtViewTaskDueDate);
        deleteSubTaskBtn = (Button) v.findViewById(R.id.deleteSubTask);

        sTitle = (EditText) v.findViewById(R.id.txtViewsubTaskTitle);
        sDescription = (EditText) v.findViewById(R.id.txtViewSubtaskDescription);
        sCheck = (CheckBox) v.findViewById(R.id.subtaskChecker);
        sDueDate = (EditText) v.findViewById(R.id.txtViewSubTaskDueDate);

        linearLayout = v.findViewById(R.id.subtaskLinearLayout);
        Button addSubTask = v.findViewById(R.id.subTaskBtn);

        tTitle.setText(taskerModel.getTaskTitle());
        tDescription.setText(taskerModel.getTaskDescription());
        tCheck.setText(String.format("Task completed: %s", taskerModel.getCompleted().toString()));
        tDueDate.setText(String.format("Task due date: %s", taskerModel.getTaskDueDate()));

        if (taskerModel.getHasSubTask()) {
            sTitle.setText(subTaskModel.getSubTaskTitle());
            addSubTask.setVisibility(View.GONE);
            linearLayout.setVisibility(View.VISIBLE);
            sDescription.setText(subTaskModel.getSubTaskDescription());
            if (subTaskModel.getSubTaskCompleted() != null) {
                sCheck.setChecked(subTaskModel.getSubTaskCompleted());
                sCheck.setText("Completed: ");
            }
           sDueDate.setText(subTaskModel.getSubTaskDueDate());
        } else {
            addSubTask.setVisibility(View.VISIBLE);
            addSubTask.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    linearLayout.setVisibility(View.VISIBLE);

                }
            });
        }


    }


}
