package gent.polymath.fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import gent.polymath.R;
import gent.polymath.activities.SingleActivityContent;
import gent.polymath.adapters.TasksAdapter;
import gent.polymath.interfaces.ClickListener;
import gent.polymath.interfaces.RecyclerTouchListener;
import gent.polymath.models.SubTask;
import gent.polymath.models.TaskerModel;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;


public class TaskFragment extends Fragment {


    private OnFragmentInteractionListener mListener;
    private ScrollView scrollView;
    private RecyclerView recyclerView;
    private LinearLayout subTaskLinearLayout;

    private EditText txtNewTaskTitle;
    private EditText txtNewTaskDescription;
    private EditText txtNewSubTaskTitle;
    private EditText txtNewSubTaskDescription;

    private CheckBox taskCheckBox;
    private CheckBox subTaskCheckBox;
    private TasksAdapter tasksAdapter;
    private Button subTaskBtn;
    private Button saveTaskBtn;
    private Realm realm;
    private FloatingActionButton floatingActionButton;
    private DatePicker taskDatePicker;
    private DatePicker subTaskDatePicker;
    private Button savechanges;

    public TaskFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        realm = Realm.getDefaultInstance();
        realm.setAutoRefresh(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.task_fragment, container, false);
        initAllData(v);
        checkForData();

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (scrollView.getVisibility() == View.VISIBLE) {
                    scrollView.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    floatingActionButton.setImageResource(R.drawable.ic_add_circle_outline_black_24dp);

                } else {
                    scrollView.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    floatingActionButton.setImageResource(R.drawable.ic_arrow_back_black_24dp);
                }

            }
        });




        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Intent intent = new Intent(getContext(), SingleActivityContent.class);
                TaskerModel realmObject = realm.where(TaskerModel.class).equalTo("id", position + 1).findFirst();

                String primarykey = null;
                if (realmObject != null) {
                    primarykey = String.valueOf(realmObject.getId());
                }
                intent.putExtra("id", primarykey);
                startActivity(intent);

            }

            @Override
            public void onLongClick(View view, int position) {
                showFailPopup(position,view);
            }
        }));

        subTaskBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (subTaskLinearLayout.getVisibility() == View.VISIBLE) {
                    subTaskLinearLayout.setVisibility(View.GONE);

                } else {
                    subTaskLinearLayout.setVisibility(View.VISIBLE);

                }
            }
        });


        saveTaskBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = -1;
                saveTaskData(position);
                scrollView.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                floatingActionButton.setImageResource(R.drawable.ic_add_circle_outline_black_24dp);
            }
        });
        return v;
    }



    private void showFailPopup(final int position, final View v) {

        final AlertDialog.Builder myDialog = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        myDialog.setMessage("Do you want to delete selected task ?");
        myDialog.create();
        myDialog.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tasksAdapter = new TasksAdapter(deleteItemFromDb(position), getActivity());
                recyclerView.setAdapter(tasksAdapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                recyclerView.setItemAnimator(new DefaultItemAnimator());
            }
        });
        myDialog.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        myDialog.show();
    }

    public List<TaskerModel> deleteItemFromDb(final int position) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<TaskerModel> result = realm.where(TaskerModel.class).equalTo("id", position + 1).findAll();
                result.deleteAllFromRealm();
            }
        });
        realm.beginTransaction();
        RealmResults<TaskerModel> alldata = realm.where(TaskerModel.class).findAll();
        List<TaskerModel> t = realm.copyFromRealm(alldata);
        for (int i = 0; i < t.size(); i++) {
            t.get(i).setId(i + 1);
        }
        alldata.deleteAllFromRealm();
        RealmList<TaskerModel> newData = new RealmList<>();
        newData.addAll(t);
        realm.copyToRealmOrUpdate(newData);
        realm.commitTransaction();
        return newData;

    }


    public void initAllData(View view) {

        savechanges = (Button) view.findViewById(R.id.saveChanges);
        taskDatePicker = (DatePicker) view.findViewById(R.id.taskDatePicker);
        subTaskDatePicker = (DatePicker) view.findViewById(R.id.subTaskDatePicker);
        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.floatActionBtn);
        saveTaskBtn = (Button) view.findViewById(R.id.saveSubTask);
        subTaskBtn = (Button) view.findViewById(R.id.subTaskBtn);
        taskCheckBox = (CheckBox) view.findViewById(R.id.taskChecker);
        subTaskCheckBox = (CheckBox) view.findViewById(R.id.subTaskChecker);
        scrollView = (ScrollView) view.findViewById(R.id.newTaskScrollView);
        subTaskLinearLayout = (LinearLayout) view.findViewById(R.id.subtaskLinearLayout);
        recyclerView = (RecyclerView) view.findViewById(R.id.allTasksRecyclerView);
        txtNewTaskTitle = (EditText) view.findViewById(R.id.txtTaskTitle);
        txtNewTaskDescription = (EditText) view.findViewById(R.id.txtTaskDescription);
        txtNewSubTaskTitle = (EditText) view.findViewById(R.id.txtsubTaskTitle);
        txtNewSubTaskDescription = (EditText) view.findViewById(R.id.txtSubtaskDescription);
        scrollView.setVisibility(View.GONE);
        subTaskLinearLayout.setVisibility(View.GONE);

    }

    public void saveTaskData(int position) {
        RealmList<TaskerModel> allTasks = new RealmList<>();
        TaskerModel task;
        showExistingDataObject(position);
        realm.beginTransaction();
        if (position == -1) {
             task = realm.createObject(TaskerModel.class, setPrimaryKey());
        } else {
             task = realm.createObject(TaskerModel.class, position+1);

        }
        SubTask subTask = realm.createObject(SubTask.class,setSubTaskPrimaryKey());

        task.setTaskTitle(txtNewTaskTitle.getText().toString());
        task.setTaskDescription(txtNewTaskDescription.getText().toString());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = sdf.format(getDateFromDatePicker(taskDatePicker));
        task.setTaskDueDate(dateString);
        if (taskCheckBox.isChecked()) {
            task.setCompleted(true);
        } else {
            task.setCompleted(false);
        }
        if (txtNewSubTaskTitle.getText().toString().equals("") &&
                txtNewSubTaskTitle.getText().toString().trim().isEmpty() &&
                txtNewSubTaskDescription.getText().toString().equals("") &&
                txtNewSubTaskDescription.getText().toString().trim().isEmpty()) {
            task.setHasSubTask(false);
        } else {
            task.setHasSubTask(true);
            if (subTaskCheckBox.isChecked()) {
                subTask.setSubTaskCompleted(true);
            } else {
                subTask.setSubTaskCompleted(false);
            }
            subTask.setSubTaskTitle(txtNewSubTaskTitle.getText().toString());
            subTask.setSubTaskDescription(txtNewSubTaskTitle.getText().toString());
            String subtaskDate = sdf.format(getDateFromDatePicker(subTaskDatePicker));
            subTask.setSubTaskDueDate(subtaskDate);
            task.setSubTask(subTask);
        }
        allTasks.add(task);
        realm.copyToRealmOrUpdate(allTasks);
        RealmResults<TaskerModel> finder = realm.where(TaskerModel.class).findAll();
        realm.commitTransaction();
        tasksAdapter = new TasksAdapter(finder, getActivity());
        recyclerView.setAdapter(tasksAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }


    public void showExistingDataObject(final int position) {
        realm.beginTransaction();
        TaskerModel object = realm.where(TaskerModel.class).equalTo("id", position + 1).findFirst();

        if (object != null) {
            txtNewTaskTitle.setText(object.getTaskTitle());
            txtNewTaskDescription.setText(object.getTaskTitle());
            txtNewTaskTitle.setText(object.getTaskTitle());
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (object != null) {
                Date date = sdf.parse(object.getTaskDueDate());
                taskDatePicker.updateDate(date.getYear(), date.getMonth(), date.getDay());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (object != null) {
            if (object.getCompleted()) {
                taskCheckBox.setChecked(true);
            } else {
                taskCheckBox.setChecked(false);
            }
        }
        if (object != null && object.getHasSubTask()) {
            subTaskLinearLayout.setVisibility(View.VISIBLE);
            object.setHasSubTask(true);
            txtNewSubTaskTitle.setText(object.getSubTask().getSubTaskTitle());
            txtNewSubTaskDescription.setText(object.getSubTask().getSubTaskDescription());
            try {
                if (object.getSubTask() != null) {
                    Date date = sdf.parse(object.getSubTask().getSubTaskDueDate());
                    subTaskDatePicker.updateDate(date.getYear(), date.getMonth(), date.getDay());
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if (object != null) {
            object.deleteFromRealm();
        }
        realm.commitTransaction();
    }



    public static Date getDateFromDatePicker(DatePicker datePicker) {
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);


        return calendar.getTime();
    }

    public int setPrimaryKey() {
        Number id = realm.where(TaskerModel.class).max("id");
        return (id == null) ? 1 : id.intValue() + 1;
    }

    public int setSubTaskPrimaryKey() {
        Number id = realm.where(SubTask.class).max("id");
        return (id == null) ? 1 : id.intValue() + 1;
    }

    public void checkForData() {
        realm.beginTransaction();
        RealmResults<TaskerModel> alldata = realm.where(TaskerModel.class).findAll();
        tasksAdapter = new TasksAdapter(alldata, getActivity());
        recyclerView.setAdapter(tasksAdapter);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        realm.commitTransaction();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
